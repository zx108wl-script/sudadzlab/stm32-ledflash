#ifndef __MAIN_H_
#define __MAIN_H_

#include "stm32f10x.h"
#include "bsp_led.h"
#include "bsp_SysTick.h"

typedef union{
    struct
    {
        uint8_t b_0:1;
        uint8_t b_1:1;
        uint8_t b_2:1;
        uint8_t b_3:1;
        uint8_t b_4:1;
        uint8_t b_5:1;
        uint8_t b_6:1;
        uint8_t b_7:1;
    }byte;
    uint8_t data;
}Flag8;

#endif