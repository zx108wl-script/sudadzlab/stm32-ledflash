
#include "main.h"
extern uint16_t SysTim;
extern Flag8 flag;

void SysTick_Handler(void)
{
	SysTim += 1;
	if(SysTim == 2000){
		SysTim = 0;
		flag.byte.b_0 = 1;
	}
}
