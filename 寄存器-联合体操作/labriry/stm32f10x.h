//使用到的STM32F103的寄存器地址
#ifndef __STM32F10X_H__
#define	__STM32F10X_H__

typedef     unsigned char       uint8_t;
typedef     unsigned short      uint16_t;
typedef     unsigned long       uint32_t;
typedef     unsigned long long  uint64_t;

// 定义寄存器
#define                 RCC_BASE                     (uint32_t)(0x40021000)    // RCC寄存器地址映像
//GPIO寄存器地址映像
#define                 GPIOB_BASE                   (uint32_t)(0x40010C00)    //GPIOB
#define                 GPIOC_BASE                   (uint32_t)(0x40010800)    //GPIOC
//AFIO寄存器地址映像
#define                 AFIO_BASE                    (uint32_t)(0x40010000)
//NVIC寄存器地址映像
#define                 NVIC_BASE                    (uint32_t)(0xE000E000)
//SysTick寄存器地址映像
#define                 SysTick_BASE                 (uint32_t)(0xE000E010)



typedef struct{
    uint32_t CR;
    uint32_t CFGR;
    uint32_t CIR;
    uint32_t APB2RSTR;
    uint32_t APBIRSTR;
    uint32_t AHBENR;
    uint32_t APB2ENR;
    uint32_t APB1ENR;
    uint32_t BDCR;
    uint32_t CSR;
}RCC_TypeDef;

typedef struct{
    uint32_t CRL;
    uint32_t CRH;
    uint32_t IDR;
    uint32_t ODR;
    uint32_t BSRR;
    uint32_t BRR;
    uint32_t LCKR;
}GPIO_TypeDef;

typedef struct{
    uint32_t EVCR;
    uint32_t MAPR;
    uint32_t EXTICR1;
    uint32_t EXTICR2;
    uint32_t EXTICR3;
    uint32_t EXTICR4;
}AFIO_TypeDef;

typedef struct
{
    uint32_t SETEN;         // SETENA寄存器阵列的起始地址
    uint32_t IROPRI;        // 中断优先级斋存器阵列的起始地址
    uint32_t VECTTBL;       // 向量表偏移寄存器的地址
    uint32_t AIRCR;         // 应用程序中断及复位控制寄存器的地址
}NVIC_TypeDef;

typedef struct
{
    uint32_t CSR;
    uint32_t RVR;
}SysTick_TypeDef;



#define             RCC                 ((RCC_TypeDef*)             RCC_BASE        )
#define             GPIOB               ((GPIO_TypeDef*)            GPIOB_BASE      )
#define             GPIOC               ((GPIO_TypeDef*)            GPIOC_BASE      )
#define             AFIO                ((AFIO_TypeDef*)            AFIO_BASE       )
#define             NVIC                ((NVIC_TypeDef*)            NVIC_BASE       )
#define             SysTick             ((SysTick_TypeDef*)         SysTick_BASE    )

#define             BitAlias_BASE        (uint32_t )(0x22000000)    //位带别名区起始地址
#define             SETENA0             *(uint32_t*)(0xE000E100)
#define             SETENA1             *(uint32_t*)(0xE000E104)


//FLASH缓冲寄存器地址映像
#define             FLASH_ACR           *(uint32_t*)(0x40022000)

#define             MSP_TOP             *(uint32_t*)(0x20005000)    //主堆栈起始值
#define             PSP_TOP             *(uint32_t*)(0x20004E00)    //进程堆栈起始值


typedef union{
    struct
    {
        uint32_t b_0:1;
        uint32_t b_1:1;
        uint32_t b_2:1;
        uint32_t b_3:1;
        uint32_t b_4:1;
        uint32_t b_5:1;
        uint32_t b_6:1;
        uint32_t b_7:1;
        uint32_t b_8:1;
        uint32_t b_9:1;
        uint32_t b_10:1;
        uint32_t b_11:1;
        uint32_t b_12:1;
        uint32_t b_13:1;
        uint32_t b_14:1;
        uint32_t b_15:1;
        uint32_t b_16:1;
        uint32_t b_17:1;
        uint32_t b_18:1;
        uint32_t b_19:1;
        uint32_t b_20:1;
        uint32_t b_21:1;
        uint32_t b_22:1;
        uint32_t b_23:1;
        uint32_t b_24:1;
        uint32_t b_25:1;
        uint32_t b_26:1;
        uint32_t b_27:1;
        uint32_t b_28:1;
        uint32_t b_29:1;
        uint32_t b_30:1;
        uint32_t b_31:1;
    }byte;
    uint32_t data;
}Flag32;

typedef union{
    struct
    {
        uint16_t b_0:1;
        uint16_t b_1:1;
        uint16_t b_2:1;
        uint16_t b_3:1;
        uint16_t b_4:1;
        uint16_t b_5:1;
        uint16_t b_6:1;
        uint16_t b_7:1;
        uint16_t b_8:1;
        uint16_t b_9:1;
        uint16_t b_10:1;
        uint16_t b_11:1;
        uint16_t b_12:1;
        uint16_t b_13:1;
        uint16_t b_14:1;
        uint16_t b_15:1;
    }byte;
    uint16_t data;
}Flag16;

typedef union{
    struct
    {
        uint8_t b_0:1;
        uint8_t b_1:1;
        uint8_t b_2:1;
        uint8_t b_3:1;
        uint8_t b_4:1;
        uint8_t b_5:1;
        uint8_t b_6:1;
        uint8_t b_7:1;
    }byte;
    uint8_t data;
}Flag8;
#endif