#include "../labriry/stm32f10x.h"


//变量定义
uint16_t SysTim = 0;
Flag8 flag;

int main (void)
{
	// 启动外部M晶振
	RCC->CR |= ((1)<<16);
	while (RCC->CR & ((1)<<17));//等待设置完成
	RCC->CR |= ((1)<<17);		//标志位复位
	// Flash缓冲
	FLASH_ACR = 0x00000032;
	// 设置PLL锁相环倍率为9，HSE输入不分频
	RCC->CFGR |= (((1)<<18) | ((1)<<19) | ((1)<<20) | ((1)<<16) | ((1)<<14));
	RCC->CFGR |= ((1)<<10);
	// 启动PLL锁相环
	RCC->CR |= ((1)<<24);
	while (RCC->CR & ((1)<<25));//等待设置完成
	RCC_CR |= ((1)<<25);		//标志位复位
	//选择PLL时钟作为系统时钟
	RCC->CFGR |= (((1)<<18) | ((1)<<19) | ((1)<<20) | ((1)<<16) | ((1)<<14));
	RCC->CFGR |= ((1)<<10);
	RCC->CFGR |= ((1)<<1);
	//其它RCC相关设置
	RCC->APB2ENR = ((1)<<3);	//开启GPIOB端口时钟
	// IO端口设置
	GPIOB->CRL &= ~( 0x0F<< (4*0));		//清空控制PB0的端口位
	GPIOB->CRL |= (1<<4*0);	//PB0通用推挽输出模式，速度10M
	//SysTick参数设置
	SysTick->RVR = 9000;	//SysTick装初值
	SysTick->CSR = 0x03;	//设定、启动SysTick
	//切换成用户级线程序模式
	//PSP_TOP
	//初始化SRAM、寄存器（）
	flag.data = 0;
	SysTim = 0;
	while(1){
		if(flag.byte.b_0){	//Flag1 & ((1)<<1)
			flag.byte.b_0 = 0;
			if(flag.byte.b_1 & ((1)<<0)){	//Flag1 & ((1)<<0)
				flag.byte.b_1 = 0;
				GPIOB->BRR |= ((1)<<0);		//PB.0输出0
			}
			else{
				flag.byte.b_1 = 1;
				GPIOB->BSRR |= ((1)<<0);		//PB.0输出1
			}
		}
	}
}

void SysTick_Handler(void)
{
	SysTim += 1;
	if(SysTim == 1000){
		SysTim = 0;
		flag.byte.b_0 = 1;
	}
}


void SystemInit(void)
{
	// 函数体为空，目的是为了骗过编译器不报错
}

