#	闪烁的LED灯——STM32CubeMX生成

##	文件结构

├─Core		**代码文件**
│  ├─Inc		**头文件**
│  ├─Src		**c代码目录**
│  └─StartUp	**启动文件（需要自己复制）**
├─Drivers		**驱动代码，不管**
│  ├─CMSIS
│  │  ├─Device
│  │  │  └─ST
│  │  │      └─STM32F1xx
│  │  │          ├─Include
│  │  │          └─Source
│  │  │              └─Templates
│  │  └─Include
│  └─STM32F1xx_HAL_Driver
│      ├─Inc
│      │  └─Legacy
│      └─Src
└─MDK-ARM	**程序启动目录**
    ├─DebugConfig
    ├─LED
    └─RTE
        └─_LED



## 主要代码

由于整个项目由STM32CubeMX生成，所以配置代码不需要自己写，自己去理解，主要讲几个增加代码的地方。

1. SysTick时钟默认为1ms的执行频率，不需要修改，如果修改，需要前往文件“**stm32f1xx_hal.c**”文件里**237行**修改

   ```c
     if (HAL_SYSTICK_Config(SystemCoreClock / (1000U / uwTickFreq)) > 0U)
     {
       return HAL_ERROR;
     }
   ```

   将1000U修改成合适的数值。

2. 主程序电灯代码还是放在“**main.c**”文件的“**main**”函数的“**while(1)**”循环里。

   ```c
   int main(void)
   {
   
     /* USER CODE BEGIN 1 */
   
     /* USER CODE END 1 */
   
     /* MCU Configuration--------------------------------------------------------*/
   
     /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
     HAL_Init();
   
     /* USER CODE BEGIN Init */
   
     /* USER CODE END Init */
   
     /* Configure the system clock */
     SystemClock_Config();
   
     /* USER CODE BEGIN SysInit */
   
     /* USER CODE END SysInit */
   
     /* Initialize all configured peripherals */
     MX_GPIO_Init();
     /* USER CODE BEGIN 2 */
   	flag.data = 0;					//初始化变量
   	SysTim = 0;
     /* USER CODE END 2 */
   
     /* Infinite loop */
     /* USER CODE BEGIN WHILE */
     while (1)
     {
       /* USER CODE END WHILE */
   		if(flag.byte.b_0){
   			flag.byte.b_0 = 0;
   			if(flag.byte.b_1){
   				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, 0);
   				flag.byte.b_1 = 0;
   			}
   			else{
   				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, 1);
   				flag.byte.b_1 = 1;
   			}
   		}
       /* USER CODE BEGIN 3 */
     }
     /* USER CODE END 3 */
   }
   
   ```

   

3. 系统中断函数在 “**stm32f1xx_it.c**”文件里**185行**

   ```c
   void SysTick_Handler(void)
   {
     /* USER CODE BEGIN SysTick_IRQn 0 */
   
     /* USER CODE END SysTick_IRQn 0 */
     HAL_IncTick();
     /* USER CODE BEGIN SysTick_IRQn 1 */
   	SysTim += 1;
   	if(SysTim == 500){
   		SysTim = 0;
   		flag.byte.b_0 = 1;
   	}
     /* USER CODE END SysTick_IRQn 1 */
   }
   ```

   

4. 这里使用到了跨文件的全局变量。

    - 变量声明在“**main.c**”文件里**26行**

        ```c
        uint16_t SysTim;
        Flag8 flag;
        ```

    - 变量声明全局引用在“**stm32f1xx_it.c**”文件里**29行**

        ```c
        extern uint16_t SysTim;
        extern Flag8 flag;
        ```

    - Flag8联合体声明在“**main.c**”文件里**32行**

        ```c
        typedef union{
            struct{
                uint8_t b_0:1;
                uint8_t b_1:1;
                uint8_t b_2:1;
                uint8_t b_3:1;
                uint8_t b_4:1;
                uint8_t b_5:1;
                uint8_t b_6:1;
                uint8_t b_7:1;
            }byte;
            uint8_t data;
        }Flag8;
        ```

    函数主要代码如上，操作流程看网图吧

    - [STM32——SysTick定时器（CubeMX配置SysTick）_stm32cubemx systick-CSDN博客](https://blog.csdn.net/asdf1234dfty/article/details/125539934)

    只需要把系统时钟改成我们自己的就行了