#ifndef __SYSTICK_H
#define __SYSTICK_H

#include "stm32f1xx.h"

void SysTick_Init(void);

#endif /* __SYSTICK_H */
