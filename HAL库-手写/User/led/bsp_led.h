#ifndef __LED_H
#define	__LED_H

#include "stm32f1xx.h"

//引脚定义
/*******************************************************/

// 绿色灯
#define LED_PIN                  GPIO_PIN_0               
#define LED_GPIO_PORT            GPIOB                      
#define LED_GPIO_CLK_ENABLE()   __HAL_RCC_GPIOB_CLK_ENABLE()

/************************************************************/


/** 控制LED灯亮灭的宏，
	* LED低电平亮，设置ON=0，OFF=1
	* 若LED高电平亮，把宏设置成ON=1 ，OFF=0 即可
	*/
#define ON  GPIO_PIN_RESET
#define OFF GPIO_PIN_SET

/* 带参宏，可以像内联函数一样使用 */
#define LED(a)	HAL_GPIO_WritePin(LED_GPIO_PORT,LED_PIN,a)

void LED_GPIO_Config(void);

#endif /* __LED_H */
