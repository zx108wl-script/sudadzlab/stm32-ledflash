#include "main.h"
#include "stm32f1xx.h"
#include "./led/bsp_led.h"
#include "./systick/bsp_SysTick.h"

typedef union{
	struct{
			uint8_t b_0:1;
			uint8_t b_1:1;
			uint8_t b_2:1;
			uint8_t b_3:1;
			uint8_t b_4:1;
			uint8_t b_5:1;
			uint8_t b_6:1;
			uint8_t b_7:1;
	}byte;
	uint8_t data;
}Flag8;

uint16_t SysTim;
Flag8 flag;

void SystemClock_Config(void);



int main(void)
{
  SystemClock_Config();
	LED_GPIO_Config();
	SysTick_Init();
	flag.data = 0;
	SysTim = 0;
	while(1)                            
	{
		if(flag.byte.b_0){
			flag.byte.b_0 = 0;
			if(flag.byte.b_1){
				LED(OFF);
				flag.byte.b_1 = 0;
			}
			else{
				LED(ON);
				flag.byte.b_1 = 1;
			}
		}
	}
}



void SystemClock_Config(void)
{
  RCC_ClkInitTypeDef clkinitstruct = {0};
  RCC_OscInitTypeDef oscinitstruct = {0};
  
  /* Enable HSE Oscillator and activate PLL with HSE as source */
  oscinitstruct.OscillatorType  = RCC_OSCILLATORTYPE_HSE;
  oscinitstruct.HSEState        = RCC_HSE_ON;
  oscinitstruct.HSEPredivValue  = RCC_HSE_PREDIV_DIV1;
  oscinitstruct.PLL.PLLState    = RCC_PLL_ON;
  oscinitstruct.PLL.PLLSource   = RCC_PLLSOURCE_HSE;
  oscinitstruct.PLL.PLLMUL      = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&oscinitstruct)!= HAL_OK)
  {
    /* Initialization Error */
    while(1); 
  }

  /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2 
     clocks dividers */
  clkinitstruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  clkinitstruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  clkinitstruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  clkinitstruct.APB2CLKDivider = RCC_HCLK_DIV1;
  clkinitstruct.APB1CLKDivider = RCC_HCLK_DIV2;  
  if (HAL_RCC_ClockConfig(&clkinitstruct, FLASH_LATENCY_2)!= HAL_OK)
  {
    /* Initialization Error */
    while(1); 
  }
}


void TimingDelay_Decrement(void){
	SysTim += 1;
	if(SysTim == 1000){
		SysTim = 0;
		flag.byte.b_0 = 1;
	}
}
