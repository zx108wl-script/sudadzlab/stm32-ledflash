//使用到的STM32F103的寄存器地址
#ifndef __STM32F10X_H__
#define	__STM32F10X_H__

// 定义寄存器
// RCC寄存器地址映像
#define                 RCC_BASE                     (unsigned int )(0x40021000)
#define                 RCC_CR                      *(unsigned int*)(RCC_BASE + 0x00)
#define                 RCC_CFGR                    *(unsigned int*)(RCC_BASE + 0x04)
#define                 RCC_CIR                     *(unsigned int*)(RCC_BASE + 0x08)
#define                 RCC_APB2RSTR                *(unsigned int*)(RCC_BASE + 0x0C)
#define                 RCC_APBIRSTR                *(unsigned int*)(RCC_BASE + 0x10)
#define                 RCC_AHBENR                  *(unsigned int*)(RCC_BASE + 0x14)
#define                 RCC_APB2ENR                 *(unsigned int*)(RCC_BASE + 0x18)
#define                 RCC_APB1ENR                 *(unsigned int*)(RCC_BASE + 0x1C)
#define                 RCC_BDCR                    *(unsigned int*)(RCC_BASE + 0x20)
#define                 RCC_CSR                     *(unsigned int*)(RCC_BASE + 0x24)

//GPIO寄存器地址映像
//GPIOB
#define                 GPIOB_BASE                   (unsigned int )(0x40010C00)
#define                 GPIOB_CRL                   *(unsigned int*)(GPIOB_BASE + 0x00)
#define                 GPIOB_CRH                   *(unsigned int*)(GPIOB_BASE + 0x04)
#define                 GPIOB_IDR                   *(unsigned int*)(GPIOB_BASE + 0x08)
#define                 GPIOB_ODR                   *(unsigned int*)(GPIOB_BASE + 0x0C)
#define                 GPIOB_BSRR                  *(unsigned int*)(GPIOB_BASE + 0x10)
#define                 GPIOB_BRR                   *(unsigned int*)(GPIOB_BASE + 0x14)
#define                 GPIOB_LCKR                  *(unsigned int*)(GPIOB_BASE + 0x18)
//GPIOC
#define                 GPIOC_BASE                   (unsigned int )(0x40010800)
#define                 GPIOC_CRL                   *(unsigned int*)(GPIOC_BASE + 0x00)
#define                 GPIOC_CRH                   *(unsigned int*)(GPIOC_BASE + 0x04)
#define                 GPIOC_IDR                   *(unsigned int*)(GPIOC_BASE + 0x08)
#define                 GPIOC_ODR                   *(unsigned int*)(GPIOC_BASE + 0x0C)
#define                 GPIOC_BSRR                  *(unsigned int*)(GPIOC_BASE + 0x10)
#define                 GPIOC_BRR                   *(unsigned int*)(GPIOC_BASE + 0x14)
#define                 GPIOC_LCKR                  *(unsigned int*)(GPIOC_BASE + 0x18)

//AFI0寄存器地址映像
#define                 AFIO_BASE                    (unsigned int )(0x40010000)
#define                 AFIO_EVCR                   *(unsigned int*)(AFIO_BASE + 0x00)
#define                 AFIO_MAPR                   *(unsigned int*)(AFIO_BASE + 0x04)
#define                 AFIO_EXTICR1                *(unsigned int*)(AFIO_BASE + 0x08)
#define                 AFIO_EXTICR2                *(unsigned int*)(AFIO_BASE + 0x0C)
#define                 AFIO_EXTICR3                *(unsigned int*)(AFIO_BASE + 0x10)
#define                 AFIO_EXTICR4                *(unsigned int*)(AFIO_BASE + 0x14)

//NVIC寄存器地址映像
#define                 NVIC_BASE                    (unsigned int )(0xE000E000)
#define                 NVIC_SETEN                  *(unsigned int*)(NVIC_BASE + 0x0010)    // SETENA寄存器阵列的起始地址
#define                 NVIC_IROPRI                 *(unsigned int*)(NVIC_BASE + 0x0400)    // 中断优先级斋存器阵列的起始地址
#define                 NVIC_VECTTBL                *(unsigned int*)(NVIC_BASE + 0x0D08)    // 向量表偏移寄存器的地址
#define                 NVIC_AIRCR                  *(unsigned int*)(NVIC_BASE + 0x0D0C)    // 应用程序中断及复位控制寄存器的地址

#define                 SETENA0                     *(unsigned int*)(0xE000E100)
#define                 SETENA1                     *(unsigned int*)(0xE000E104)

//SysTick寄存器地址映像
#define                 SysTick_BASE                 (unsigned int )(0xE000E010)
#define                 SYSTICKCSR                  *(unsigned int*)(SysTick_BASE + 0x00)
#define                 SYSTICKRVR                  *(unsigned int*)(SysTick_BASE + 0x04)

//FLASH缓冲寄存器地址映像
#define                 FLASH_ACR                   *(unsigned int*)(0x40022000)

#define                 MSP_TOP                     *(unsigned int*)(0x20005000)    //主堆栈起始值
#define                 PSP_TOP                     *(unsigned int*)(0x20004E00)    //进程堆栈起始值

#endif