#include "stm32f10x.h"


//变量定义
#define     BitAlias_BASE        (unsigned int )(0x22000000)    //位带别名区起始地址
#define			Flag1               *(unsigned int*)(0x20000200)
#define			b_flas              *(unsigned int*)(BitAlias_BASE + (0x200*32) + (0*4))    //位地址
#define			b_05s               *(unsigned int*)(BitAlias_BASE + (0x200*32) + (1*4))    //位地址
#define			DlyI                *(unsigned int*)(0x20000204)
#define			DlyJ                *(unsigned int*)(0x20000208)
#define			DlyK                *(unsigned int*)(0x2000020C)
#define			SysTim              *(unsigned int*)(0x20000210)

int main (void)
{
	// 启动外部M晶振
	RCC_CR |= ((1)<<16);
	while (RCC_CR & ((1)<<17));//等待设置完成
	RCC_CR |= ((1)<<17);		//标志位复位
	// Flash缓冲
	FLASH_ACR = 0x00000032;
	// 设置PLL锁相环倍率为9，HSE输入不分频
	RCC_CFGR |= (((1)<<18) | ((1)<<19) | ((1)<<20) | ((1)<<16) | ((1)<<14));
	RCC_CFGR |= ((1)<<10);
	// 启动PLL锁相环
	RCC_CR |= ((1)<<24);
	while (RCC_CR & ((1)<<25));//等待设置完成
	//RCC_CR |= ((1)<<25);		//标志位复位
	//选择PLL时钟作为系统时钟
	RCC_CFGR |= (((1)<<18) | ((1)<<19) | ((1)<<20) | ((1)<<16) | ((1)<<14));
	RCC_CFGR |= ((1)<<10);
	RCC_CFGR |= ((1)<<1);
	//其它RCC相关设置
	RCC_APB2ENR = ((1)<<3);	//开启GPIOB端口时钟
	// IO端口设置
	GPIOB_CRL &= ~( 0x0F<< (4*0));		//清空控制PB0的端口位
	GPIOB_CRL |= (1<<4*0);	//PB0通用推挽输出模式，速度10M
	//SysTick参数设置
	SYSTICKRVR = 9000;	//SysTick装初值
	SYSTICKCSR = 0x03;	//设定、启动SysTick
	//切换成用户级线程序模式
	//PSP_TOP
	//初始化SRAM、寄存器（）
	Flag1  = 0;
	DlyI   = 0;
	DlyJ   = 0;
	DlyK   = 0;
	SysTim = 0;
	while(1){
		if(Flag1 & ((1)<<1)){	//Flag1 & ((1)<<1)
			b_05s = 0;
			if(Flag1 & ((1)<<0)){	//Flag1 & ((1)<<0)
				b_flas = 0;
				GPIOB_BRR |= ((1)<<0);		//PB.0输出0
			}
			else{
				b_flas = 1;
				GPIOB_BSRR |= ((1)<<0);		//PB.0输出1
			}
		}
	}
}

void SysTick_Handler(void)
{
	SysTim += 1;
	if(SysTim == 200){
		SysTim = 0;
		b_05s = 1;
	}
}


void SystemInit(void)
{
	// 函数体为空，目的是为了骗过编译器不报错
}

